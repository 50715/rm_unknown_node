rm_unknown_node = {}

local S = minetest.get_translator("rm_unknown_node")

minetest.register_privilege("rm_unknown_node", {
    description = S("The user can delete unknown blocks"),	-- Privilege description
    give_to_singleplayer = true,							-- Whether to grant the privilege to singleplayer.
    give_to_admin = true									-- Whether to grant the privilege to the server admin.
    														-- Uses value of 'give_to_singleplayer' by default.
	}
)

minetest.register_chatcommand("rm_unknown_node", {
	params = "<radius>",
	description = S("Remove unknown nodes in "),
	privs = {rm_unknown_node=true},
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("Player not found")
		end
		local radius = tonumber(param)
		local pos1 = {}
		local pos2 = {}

		if radius and radius > 0 then
			local playerpos = vector.round(player:get_pos())
			pos1 = vector.subtract(playerpos, radius) -- low left
			pos2 = vector.add(playerpos, radius) -- top right
		else
			minetest.chat_send_player(name, S("Missing or invalid radius!"))
			return false
		end
		local count = rm_unknown_node.rm(pos1, pos2)
		minetest.chat_send_player(name, count..S(" node(s) removed!"))
		return true
	end,
})

rm_unknown_node.rm = function(pos1, pos2)
	local count = 0
	local manip = minetest.get_voxel_manip()
	local emerged_pos1, emerged_pos2 = manip:read_from_map(pos1, pos2)
	local area = VoxelArea:new({MinEdge=emerged_pos1, MaxEdge=emerged_pos2})
	local nodes = manip:get_data()
	for i in area:iterp(pos1, pos2) do
		local cur_node = minetest.get_name_from_content_id(nodes[i])
		if not minetest.registered_nodes[cur_node] then
			nodes[i] = minetest.get_content_id("air") -- replace unknown with air
			count = count + 1
		end
	end

	-- write changes to map
	manip:set_data(nodes)
	manip:write_to_map()
	manip:update_map()

	return count
end
