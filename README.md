# remove_unknown

Use this mod to remove unknown nodes from your minetest world.

Alternatively, you can use ```/rm_unknown_node <radius>```

## Original

Original mods [remove_unknown](https://github.com/indriApollo/remove_unknown.git)

## Privileges

rm_unknown_node

## Licence

GNU LGPL v3
